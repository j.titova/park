import java.util.Objects;
import java.util.Scanner;

public class Snake implements Comparable<Snake>{
    private String kind;
    private boolean acid;
    private int bodyLength;

    public Snake(String kind, boolean acid, int bodyLength) {
        this.kind = kind;
        this.acid = acid;
        this.bodyLength = bodyLength;
    }

    public void input(){
        Scanner sc = new Scanner(System.in);
        input(sc);
    }

    public void input(Scanner sc){
        System.out.print("Enter kind: ");
        this.kind = sc.nextLine();
        System.out.print("Enter acid (true or false): ");
        this.acid = sc.nextBoolean(); sc.nextLine();
        System.out.print("Enter body length: ");
        this.bodyLength = sc.nextInt();
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public boolean isEatable() {
        return acid;
    }

    public void setAcid(boolean acid) {
        this.acid = acid;
    }

    public int getAge() {
        return bodyLength;
    }

    public void setAge(int bodyLength) {
        this.bodyLength = bodyLength;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Snake snake = (Snake) o;
        return acid == snake.acid &&
                bodyLength == snake.bodyLength &&
                Objects.equals(kind, snake.kind);
    }

    Snake copy(Snake fish){
        this.kind = fish.getKind();
        this.acid = fish.isEatable();
        this.bodyLength = fish.getAge();
        return this;
    }

    @Override
    public int compareTo(Snake o) {
        return this.bodyLength-o.bodyLength;
    }

    @Override
    public String toString() {
        return "Snake{" +
                "kind='" + kind + '\'' +
                ", acid=" + acid +
                ", bodyLength=" + bodyLength +
                '}';
    }
}
