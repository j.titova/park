import java.util.Objects;
import java.util.Scanner;

public class Animal implements Comparable<Animal>{
    private String continent;
    private String kind;
    private int age;
    private char sex;

    public Animal(String continent, String kind, int age, char sex) {
        this.continent = continent;
        this.kind = kind;
        this.age = age;
        this.sex = sex;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return age == animal.age &&
                sex == animal.sex &&
                Objects.equals(continent, animal.continent) &&
                Objects.equals(kind, animal.kind);
    }

    public void input(){
        Scanner scanner = new Scanner(System.in);
        input(scanner);
    }

    void input(Scanner scanner){
        System.out.print("Enter continent: ");
        this.continent = scanner.nextLine();
        System.out.print("Enter kind: ");
        this.kind = scanner.nextLine();
        System.out.print("Enter age: ");
        this.age = scanner.nextInt();
        System.out.print("Enter sex: ");
        scanner.nextLine();
        this.sex = scanner.next().charAt(0);
    }


    @Override
    public String toString() {
        return "Animal{" +
                "continent='" + continent + '\'' +
                ", king='" + kind + '\'' +
                ", age=" + age +
                ", sex=" + sex +
                '}';
    }

    Animal copy(Animal animal){
        this.continent = animal.getContinent();
        this.kind = animal.getKind();
        this.age = animal.getAge();
        this.sex = animal.getSex();
        return this;
    }

    @Override
    public int compareTo(Animal o) {
        return this.age - o.age;
    }
}
