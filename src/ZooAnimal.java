
public class ZooAnimal extends Animal implements inZoo {
    private int incomeDate;
    private findIndex cntx = null;

    public ZooAnimal(String continent, String kind, int age, char sex, int incomeDate) {
        super(continent, kind, age, sex);
        this.incomeDate = incomeDate;
    }

    public ZooAnimal() {
        super(null, null, 0, '\0');
        this.incomeDate = 0;
    }

    public void setCntx(findIndex cntx) {
        this.cntx = cntx;
    }

    public interface findIndex{
        int getMyIndex(inZoo t);
    }

    public int getOwnerIndex() {
        return ((findIndex) cntx).getMyIndex(this);
    }

    public int getRecDate() {
        return incomeDate;
    }

    @Override
    public void setIncomeDate(int date) {
        this.incomeDate = date;
    }

    public int getIncomeDate() {
        return incomeDate;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
