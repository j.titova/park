
public class ZooSnake extends Snake implements inOceanarium, inZoo {
    private int incomeDate;
    private findIndex cntx = null;

    public ZooSnake(){
        super(null, false, 0);
        this.incomeDate = 0;
    }

    public ZooSnake(String type, boolean acid, int bodyLength, int incomeDate) {
        super(type, acid, bodyLength);
        this.incomeDate = incomeDate;
    }

    public void setCntx(findIndex cntx) {
        this.cntx = cntx;
    }

    public interface findIndex{
        int getMyIndex(inPark t);
    }

    public int getOwnerIndex() {
        return ((findIndex) cntx).getMyIndex(this);
    }

    @Override
    public void setIncomeDate(int date) {
        this.incomeDate = date;
    }

    public int getIncomeDate() {
        return incomeDate;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
