import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Park <T extends inPark> {
   private ArrayList<T> list;

    public Park(){
        this.list = new ArrayList<>();
    }

    public Park(ArrayList<T> list) {
        this.list = list;
    }

    public ArrayList<T> getList(){
        return list;
    }

    public void sort(){
        Collections.sort(this.list, new comp());
    }

    class comp implements Comparator<T> {
        @Override
        public int compare(T o1, T o2) {
            if (o1 instanceof ZooAnimal && o2 instanceof ZooAnimal)
                return ((ZooAnimal) o1).getAge() - ((ZooAnimal) o2).getAge();
            if (o1 instanceof ZooAnimal && o2 instanceof ZooFish)
                return ((ZooAnimal) o1).getAge() - ((ZooFish) o2).getAge();
            if (o1 instanceof ZooAnimal && o2 instanceof ZooSnake)
                return ((ZooAnimal) o1).getAge() - ((ZooSnake) o2).getAge();
            if (o1 instanceof ZooFish && o2 instanceof ZooAnimal)
                return ((ZooFish) o1).getAge() - ((ZooAnimal) o2).getAge();
            if (o1 instanceof ZooFish && o2 instanceof ZooFish)
                return ((ZooFish) o1).getAge() - ((ZooFish) o2).getAge();
            if (o1 instanceof ZooFish && o2 instanceof ZooSnake)
                return ((ZooFish) o1).getAge() - ((ZooSnake) o2).getAge();
            if (o1 instanceof ZooSnake && o2 instanceof ZooAnimal)
                return ((ZooSnake) o1).getAge() - ((ZooAnimal) o2).getAge();
            if (o1 instanceof ZooSnake && o2 instanceof ZooFish)
                return ((ZooSnake) o1).getAge() - ((ZooFish) o2).getAge();
            if (o1 instanceof ZooSnake && o2 instanceof ZooSnake)
                return ((ZooSnake) o1).getAge() - ((ZooSnake) o2).getAge();
            return 0;
        }
    }

    public void add() {
        Scanner sc = new Scanner(System.in);
        add(sc);
    }

//Метод не осуществляет проверку вида парка, потому что нельзя определить аргумент класса дженерика
    public void add(Scanner sc) {
        System.out.print("Enter type (animal, fish, snake): ");
        String type = sc.nextLine();
        switch (type){
            case "animal" :{
                ZooAnimal temp = new ZooAnimal(); temp.input(); this.list.add((T)temp);
                break;
            }
            case "fish" :{
                ZooFish temp = new ZooFish(); temp.input(); this.list.add((T)temp);
                break;
            }
            case "snake" :{
                ZooSnake temp = new ZooSnake(); temp.input(); this.list.add((T)temp);
                break;
            }
        }
    }


    public void add(T obj, int date){
        obj.setIncomeDate(date);
        list.add(obj);
    }

    public void add(T obj){
        list.add(obj);
    }

    public void delete(int index) throws myException{
        if (index < 0 || index >= list.size()) throw new myException("Wrong index!");
        this.list.remove(index);
    }

    public boolean delete(T obj){
        return this.list.remove(obj);
    }

    @Override
    public String toString() {
        return "Park{" +
                "list=" + list +
                '}';
    }
}
