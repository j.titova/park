import java.util.Objects;
import java.util.Scanner;

public class Fish implements Comparable<Fish>{
    private String water;
    private String kind;
    private boolean eatable;
    private int age;

    public Fish(String water, String kind, boolean eatable, int age) {
        this.water = water;
        this.kind = kind;
        this.eatable = eatable;
        this.age = age;
    }

    public String getWater() {
        return water;
    }

    public void setWater(String water) {
        this.water = water;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public boolean isEatable() {
        return eatable;
    }

    public void setEatable(boolean eatable) {
        this.eatable = eatable;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void input(){
        Scanner scanner = new Scanner(System.in);
        input(scanner);
    }

    void input(Scanner scanner){
        System.out.print("Enter water: ");
        this.water = scanner.nextLine();
        System.out.print("Enter kind: ");
        this.kind = scanner.nextLine();
        System.out.print("Enter eatable: ");
        this.eatable = Boolean.parseBoolean(scanner.nextLine());
        System.out.print("Enter age: ");
        this.age = scanner.nextInt();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Fish fish = (Fish) o;
        return eatable == fish.eatable &&
                age == fish.age &&
                Objects.equals(water, fish.water) &&
                Objects.equals(kind, fish.kind);
    }

    @Override
    public String toString() {
        return "Fish{" +
                "water='" + water + '\'' +
                ", kind='" + kind + '\'' +
                ", eatable=" + eatable +
                ", age=" + age +
                '}';
    }

    Fish copy(Fish fish){
        this.water = fish.getWater();
        this.kind = fish.getKind();
        this.eatable = fish.isEatable();
        this.age = fish.getAge();
        return this;
    }

    @Override
    public int compareTo(Fish o) {
        return this.age - o.age;
    }
}
