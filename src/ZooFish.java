
public class ZooFish extends Fish implements inOceanarium {
    private int incomeDate;
    private findIndex cntx = null;

    public ZooFish(String water, String type, boolean eatable, int age, int incomeDate) {
        super(water, type, eatable, age);
        this.incomeDate = incomeDate;
    }

    public ZooFish(){
        super(null, null, false, 0);
        this.incomeDate = 0;
    }

    public void setCntx(findIndex cntx) {
        this.cntx = cntx;
    }

    public interface findIndex{
        int getMyIndex(inOceanarium t);
    }

    public int getOwnerIndex() {
        return ((findIndex) cntx).getMyIndex(this);
    }

    public int getRecDate() {
        return incomeDate;
    }

    @Override
    public void setIncomeDate(int date) {
        this.incomeDate = date;
    }

    public int getIncomeDate() {
        return incomeDate;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
